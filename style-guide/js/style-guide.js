$ (document).ready (function () {
    $ ('.sample').append (function () {
        return '<pre class="sample-code"><code data-language="html">' + $ (this).find ('> .sample-html').html () + '</code></pre>';
    });

    $ ('.sidebar a[href*="#"]:not([href="#"])').click (
        function () {
            if (location.pathname.replace (/^\//, '') == this.pathname.replace (/^\//, '') && location.hostname == this.hostname) {
                var target = $ (this.hash);
                target = target.length ? target : $ ('[name=' + this.hash.slice (1) + ']');
                if (target.length) {
                    $ ('html, body').animate ({
                        scrollTop: target.offset ().top - target.css ("marginTop").replace ('px', '') - 50
                    }, 500);
                    return false;
                }
            }
        })
    ;

    $ ('a[data-toggle="collapse-menu"]').click (
        function () {
            $(this).parent().find('ul').toggleClass('in');
        }
    );

    $('#sample-code').click (
        function () {
            set_cookie ('sample_code', (get_cookie('sample_code') === 'false'), 365);
            update_sample_code();
        }
    );

//    update_sample_code();
    init_tooltips();
    init_popovers();
});


function update_sample_code() {
    var show_sample_code = !(get_cookie('sample_code') === 'false');

    $('.sample-code').each(
        function () {
            if(show_sample_code === true) {
                $('.sample-code').css('display', 'block');
            }
            else {
                $('.sample-code').css('display', 'none');
            }
        }
    );

    if(show_sample_code === true) {
        $ ('#sample-code .sample-code-state').addClass('fa-check');
    }
    else {
        $ ('#sample-code .sample-code-state').removeClass('fa-check');
    }
}

function init_tooltips() {
    $('[data-toggle="tooltip"]').tooltip();
}

function init_popovers() {
    $('[data-toggle="popover"]').popover();
}
