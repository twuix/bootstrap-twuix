<section>
    <h1 id="typography" class="page-header">Typography</h1>

    <article>
        <h2 id="typography-headings">Headings</h2>

        <div class="sample">
            <div class="sample-html">
    <h1>TagCommander Style Guide</h1>
    <h2>TagCommander Style Guide</h2>
    <h3>TagCommander Style Guide</h3>
    <h4>TagCommander Style Guide</h4>
    <h5>TagCommander Style Guide</h5>
    <h6>TagCommander Style Guide</h6>
            </div>
        </div>
    </article>

    <article>
        <h2 id="typography-inline-text-elements">Inline text elements</h2>

        <h3>Marked text</h3>

        <div class="sample">
            <div class="sample-html">
    You can use the mark tag to <mark>highlight</mark> text.
            </div>
        </div>

        <h3>Deleted text</h3>

        <div class="sample">
            <div class="sample-html">
    <del>This line of text is meant to be treated as deleted text.</del>
            </div>
        </div>

        <h3>Inserted text</h3>

        <div class="sample">
            <div class="sample-html">
    <ins>This line of text is meant to be treated as an addition to the document.</ins>
            </div>
        </div>

        <h3>Underlined text</h3>

        <div class="sample">
            <div class="sample-html">
    <u>This line of text will render as underlined</u>
            </div>
        </div>

        <h3>Small text</h3>

        <div class="sample">
            <div class="sample-html">
    <small>This line of text is meant to be treated as fine print.</small>
            </div>
        </div>

        <h3>Bold text</h3>

        <div class="sample">
            <div class="sample-html">
    The following snippet of text is <strong>rendered as bold text</strong>
            </div>
        </div>

        <h3>Italics text</h3>

        <div class="sample">
            <div class="sample-html">
    The following snippet of text is <em>rendered as italicized text</em>
            </div>
        </div>
    </article>
</section>