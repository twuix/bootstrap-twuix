<!DOCTYPE html>
<html lang="en" class="<?= $page_classes; ?>">
<head>
    <?php $this->load->view('common/head'); ?>
</head>
<body>
    <header>
        <?php $this->load->view('common/header'); ?>
    </header>

    <?php if(file_exists('application/views/' . $content_view . '.php')): ?>
        <?php $this->load->view($content_view); ?>
    <?php else: ?>
        <?php $this->load->view('common/no_content_error'); ?>
    <?php endif; ?>

    <footer>
        <?php $this->load->view('common/footer'); ?>
    </footer>
</body>
</html>
