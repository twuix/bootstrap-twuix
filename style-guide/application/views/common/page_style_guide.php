<!DOCTYPE html>
<html lang="en" class="<?= $page_classes; ?>">
<head>
    <?php $this->load->view('common/head'); ?>
</head>
<body>
<header>
    <?php $this->load->view('common/header'); ?>
</header>

<div id="content" class="container">
    <aside class="sidebar" style="float: left;margin-top: 40px; position: fixed">
        <?php $this->load->view("$url_segments[0]/sidebar"); ?>
    </aside>
    <div style="padding-left: 250px">
        <?php $this->load->view("$url_segments[0]/content"); ?>
    </div>
</div>

    <footer>
        <?php $this->load->view('common/footer'); ?>
    </footer>
</body>
</html>