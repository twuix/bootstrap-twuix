<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Components extends TX_Controller {

	public function index()
	{
        $this->url_segments = array('components');
        $this->load->view('common/page_style_guide', $this->init_data());
	}
}
