<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Styles extends TX_Controller {

	public function index()
	{
        $this->url_segments = array('styles');
        $this->load->view('common/page_style_guide', $this->init_data());
	}
}
