var gulp = require ('gulp');
var autoprefixer = require ('gulp-autoprefixer');
var csscomb = require ('gulp-csscomb');
var csso = require ('gulp-csso');
var del = require ('del');
var less = require ('gulp-less');
var options = require ('gulp-options');
var preservetime = require ('gulp-preservetime');
var rename = require ('gulp-rename');
var util = require ('gulp-util');

gulp.task ('clean-css', function () {
    var destination = options.has ('production') ? 'dist' : 'src';
    return del ([destination + '/css']);
});

gulp.task ('clean-less', function () {
    if (options.has ('production')) {
        return del (['dist/src/less']);
    }
});

gulp.task ('copy-less', ['clean-less'], function () {
    if (options.has ('production')) {
        return gulp.src ('src/less/**/*')
            .pipe (gulp.dest ('dist/src/less'));
    }
});

gulp.task ('css', ['clean-css'], function () {
    var destination = options.has ('production') ? 'dist' : 'src';
    gulp.src ('src/less/bootstrap-twuix.less')
        .pipe (less ().on ('error', util.log))
        .pipe (csscomb ())
        .pipe (autoprefixer ())
        .pipe (gulp.dest (destination + '/css/'));

    if(!options.has('production')) {
        return gulp.src ('src/less/style-guide.less')
            .pipe (less ().on ('error', util.log))
            .pipe (csscomb ())
            .pipe (autoprefixer ())
            .pipe (gulp.dest (destination + '/css/'));
    }
});

gulp.task ('watch', function () {
    gulp.watch ('src/less/*.less', ['css']);
});

gulp.task ('build', ['css', 'copy-less'], function () {
    if (options.has ('production')) {
        del (['dist/src/less/style-guide.less']);
        return gulp.src ('dist/css/bootstrap-twuix.css')
            .pipe (csso ())
            .pipe (gulp.dest ('dist/css'));
    }
});

gulp.task ('default', ['build']);