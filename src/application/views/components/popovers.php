<section>
    <h1 id="popovers" class="page-header">Popovers</h1>

    <article>

        <div class="sample">
            <div class="sample-html">
    <a tabindex="0" class="btn btn-lg btn-default" role="button" data-toggle="popover" data-trigger="focus" title="My popover" data-content="And here's some amazing content. It's very engaging. Right?">Display</a>
            </div>
        </div>
    </article>
</section>