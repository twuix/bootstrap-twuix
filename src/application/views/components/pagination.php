<section id="pagination-examples">
    <h1 id="pagination" class="page-header">Pagination</h1>

    <article>
        <h2 id="pagination-default">Default</h2>

        <div class="sample">
            <div class="sample-html">
    <ul class="pagination">
        <li><a href="#">&laquo;</a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">&raquo;</a></li>
    </ul>
            </div>
        </div>
    </article>

    <article>
        <h2 id="pagination-sizing">Sizing</h2>

        <div>
            <ul class="pagination pagination-lg">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">»</a></li>
            </ul>
        </div>
        <div>
            <ul class="pagination">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">»</a></li>
            </ul>
        </div>
        <div>
            <ul class="pagination pagination-sm">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">»</a></li>
            </ul>
        </div>

        <pre class="sample-code">
            <code data-language="html">
    <ul class="pagination pagination-lg">...</ul>
    <ul class="pagination">...</ul>
    <ul class="pagination pagination-sm">...</ul></code>
        </pre>
    </article>
</section>