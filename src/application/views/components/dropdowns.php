<section>
    <h1 id="dropdowns" class="page-header">Dropdowns</h1>

    <article>
        <h2 id="dropdowns-basic">Basic</h2>

        <div class="sample">
            <div class="sample-html sample-dropdowns-basic">
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
            Dropdown <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
            <li role="presentation" class="divider"></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
        </ul>
    </div>
            </div>
        </div>
    </article>

    <article>
        <h2 id="dropdowns-headers">Headers</h2>

        <div class="sample-dropdowns-headers">
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle sr-only" type="button" id="dropdownMenu2" data-toggle="dropdown">
                    Dropdown <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                    <li role="presentation" class="dropdown-header">Dropdown header</li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation" class="dropdown-header">Dropdown header</li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
            </div>
        </div>

        <pre class="sample-code">
            <code data-language="html">
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
        <li role="presentation" class="dropdown-header">Dropdown header</li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
        ...
    </ul></code>
        </pre>
    </article>

    <article>
        <h2 id="dropdowns-disabled-menu-items">Disabled menu items</h2>

        <div class="sample-dropdowns-disabled-menu-items">
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle sr-only" type="button" id="dropdownMenu3" data-toggle="dropdown">
                    Dropdown <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Regular link</a></li>
                    <li role="presentation" class="disabled"><a role="menuitem" tabindex="-1" href="#">Disabled link</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another link</a></li>
                </ul>
            </div>
        </div>

        <pre class="sample-code">
            <code data-language="html">
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Regular link</a></li>
        <li role="presentation" class="disabled"><a role="menuitem" tabindex="-1" href="#">Disabled link</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another link</a></li>
    </ul></code>
        </pre>
    </article>
</section>