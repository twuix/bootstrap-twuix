<div class="container inner">
    <div role="alert" class="alert alert-danger alert-dismissible fade in">
        <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
        <p><strong>Oups!!!</strong><br>File <?= 'application/views/' . $content_view . '.php'; ?> not found...</p>
    </div>
</div>
