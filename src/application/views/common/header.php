<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Bootstrap Twuix</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li<?php if($url_segments[0] == 'css') {echo ' class="active"';} ?>><a href="/styles">CSS</a></li>
                <li<?php if($url_segments[0] == 'components') {echo ' class="active"';} ?>><a href="/components">Components</a></li>
                <li<?php if($url_segments[0] == 'more') {echo ' class="active"';} ?>><a href="/more">More</a></li>
            </ul>
        </div>
    </div>
</nav>