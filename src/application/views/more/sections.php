<section id="sections-examples">
    <h1 class="page-header">Section</h1>

    <article>
        <h2 id="divided-heading-example">Example</h2>

        <div class="sample">
            <div class="sample-html">
    <section class="section section-primary">Primary section</section>
    <section class="section section-secondary">Secondary section</section>
    <section class="section section-dark">Dark section</section>
    <section class="section section-gray">Gray section</section>
            </div>
        </div>
    </article>
</section>