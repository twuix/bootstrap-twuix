<section>
    <h1 id="css" class="page-header">CSS</h1>

    <article>
        <h2 id="divided-heading-example">Example</h2>

        <div class="sample">
            <div class="sample-html">
    <div class="bg-primary">Background primary</div>
    <div class="bg-secondary">Background secondary</div>
    <div class="bg-dark">Background dark</div>
    <div class="bg-gray">Background gray</div>
            </div>
        </div>
    </article>
</section>