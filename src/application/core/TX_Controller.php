<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TX_Controller extends CI_Controller
{
    var $nav;
    var $url_segments;
    var $css_files;
    var $js_files;

    function __construct()
    {
        parent::__construct();

        $this->url_segments = array();
        $segment_index = 1;
        while ($this->uri->segment($segment_index, 0) != null) {
            array_push($this->url_segments, $this->uri->segment($segment_index, 0));
            $segment_index++;
        }

//        $this->nav = json_decode(file_get_contents('database/nav.json'))[0]->items;

        // css files
        $this->css_files = array();
        array_push($this->css_files, '/vendors/font-awesome.min.css');
        array_push($this->css_files, '/vendors/rainbow/github.css');
        array_push($this->css_files, '/vendors/animate.min.css');
        array_push($this->css_files, '/vendors/bootstrap.min.css');
        array_push($this->css_files, '/css/bootstrap-twuix.css');
        array_push($this->css_files, '/css/style-guide.css');

        // javascript files
        $this->js_files = array();
        array_push($this->js_files, '/vendors/jquery.min.js');
        array_push($this->js_files, '/vendors/bootstrap.min.js');
        array_push($this->js_files, '/vendors/rainbow/rainbow.min.js');
        array_push($this->js_files, '/vendors/rainbow/generic.js');
        array_push($this->js_files, '/vendors/rainbow/html.js');
        array_push($this->js_files, '/js/style-guide.js');

    }

    public function index()
    {
        $this->load->view('common/page_default', $this->init_data());
    }

    protected function init_data()
    {
        $data = array();

        // page title
        $data['page_title'] = 'Bootstrap Twuix';

        // url
        $data['url_segments'] = $this->url_segments;

        // nav
        $data['nav'] = $this->nav;

        // page classes
        $data['page_classes'] = 'bootstrap-twuix';
        foreach($this->url_segments as $segment) {
            if($data['page_classes']) {
                $data['page_classes'] = $data['page_classes'] . ' ' . $segment;
            }
        }

        // content
        $data['content_view'] = $this->url_segments[0];
        for($i = 1; $i < count($this->url_segments); $i++) {
            $data['content_view'] .= '/' . $this->url_segments[$i];
        }
        $data['content_view'] .= '/content';


        $data['css_files'] = $this->css_files;
        $data['js_files'] = $this->js_files;

        return $data;
    }

    /**
    public function _remap($method)
    {
        $this->index();
    }
     * */
}